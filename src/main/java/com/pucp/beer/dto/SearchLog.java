package com.pucp.beer.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity()
public class SearchLog implements Serializable {
    private static final long serialVersionUID = 1084635409017778641L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String serviceRoute;
    private Date executeTime;
    private String responseTime;

    public SearchLog() {
    }

    public SearchLog(String serviceRoute, Date executeTime, String responseTime) {
        this.serviceRoute = serviceRoute;
        this.executeTime = executeTime;
        this.responseTime = responseTime;
    }

    public String getServiceRoute() {
        return serviceRoute;
    }

    public Date getExecuteTime() {
        return executeTime;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setServiceRoute(String serviceRoute) {
        this.serviceRoute = serviceRoute;
    }

    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }
}
