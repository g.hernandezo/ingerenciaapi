package com.pucp.beer.service;

import com.pucp.beer.dao.SearchLogRepository;
import com.pucp.beer.dto.SearchLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchLogService {
    @Autowired
    private SearchLogRepository dao;

    public SearchLog save(SearchLog searchLog) {
        return dao.saveAndFlush(searchLog);
    }
}
